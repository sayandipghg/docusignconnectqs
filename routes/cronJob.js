var cron = require('node-cron');
var shell = require('../routes/docuSignMain');

var apiConfig = '../config.json';
var fs = require('fs');
var private_key = fs.readFileSync(apiConfig);
var apiCreds = JSON.parse(private_key);

cron.schedule('*/'+apiCreds['SCHEDULE_JOB_TIME']+' * * * *', function () {
    console.log('Inside scheduler ');
    shell.DocusignMain(function (err) {
        if(!err) {
            console.log('DocusignMain invoked');
        }else{
            console.log('DocusignMain cannot be invoked');
        }
    });
});
