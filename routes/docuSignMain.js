var processor = require("../routes/processor");
var operation = require("../routes/accountOperations");
var notification = require("../routes/notification");

//-----------Logger Implementation-----------

var logConfig = '../logConfig.json';
var fileSys = require('fs');
var log = fileSys.readFileSync(logConfig);
var logCreds = JSON.parse(log);
var logger = require(logCreds['LOG_TYPE']).createSimpleLogger(logCreds['LOG_PATH']);
logger.setLevel(logCreds['LOG_MODE']);
//-----------Logger Implementation-----------


exports.DocusignMain = function () {
    logger.debug("Inside SDK");
    console.log('Inside SDK ');
    var apiConfig = '../config.json';
    var fs = require('fs');
    var private_key = fs.readFileSync(apiConfig);
    var apiCreds = JSON.parse(private_key);
    var requestArray = "";
    var subscription_status = 1;
    try {
        operation.listRequests(apiCreds['API-KEY'], apiCreds['Request-URL'], apiCreds['VENDOR_API_KEY'], "pending", function (err, res) {
            if (!err && res['statusMessage'].includes("OK")) {
                requestArray = JSON.parse(res['body']);
                logger.debug("List request retrieved Successfully with request length " + requestArray.length);
                console.log("List request retrieved Successfully with request length " + requestArray.length);
                requestArray = JSON.parse(res['body']);
                if (requestArray.length > 0) {
                    requestArray.forEach(function (requestArray) {
                        try {
                            console.log("Executing for request id : " + requestArray['id']);
                            if (requestArray.asset.product.id.includes(apiCreds['PRODUCT_ID'])) {
                                if (requestArray.asset.items.length > 0) {
                                    processor.processRequest(requestArray, function (err, res, SKUQuantityFlag, plan_id,requestType) {
                                        if (res != null || (res != null && res.includes("partner_account_user_exists"))) {
                                            subscription_status = 0;
                                            if (SKUQuantityFlag === 0) {
                                                operation.approveRequest(apiCreds['PROVISION_TEMPLATE_ID'], apiCreds['Request-URL'], apiCreds['VENDOR_API_KEY'], requestArray['id'], function (err, res) {
                                                    subscription_status = 0;
                                                    if (!err && res['statusMessage'].includes("OK")) {
                                                        console.log("Approved Successfully");
                                                        logger.debug("Approved Successfully");
                                                    } else {
                                                        console.log("Approval failed");
                                                        logger.error("Approval failed");
                                                    }
                                                });
                                            }
                                            notification.sendNotification(apiCreds, subscription_status, plan_id, requestArray['id'], SKUQuantityFlag,requestType, function (error, response) {
                                                if (!error && response['statusMessage'].includes("OK")) {
                                                    console.log("Notifications Successfully sent");
                                                    logger.debug("Notifications Successfully sent");

                                                } else {
                                                    console.log("Notification sending failed");
                                                    logger.error("Notification sending failed");
                                                }
                                            });
                                        } else {
                                            logger.error("process failed with error ", err);
                                            if (!err.includes("ECONNRESET")) {
                                                operation.failRequest(err, apiCreds['Request-URL'], apiCreds['VENDOR_API_KEY'], requestArray['id'], function (err, res) {
                                                    if (!err && res['statusMessage'].includes("OK")) {
                                                        console.log("Request Failed");
                                                        logger.debug("Request Failed");
                                                    } else {
                                                        console.log("Failure unsuccessful");
                                                        logger.error("Failure unsuccessful");
                                                    }
                                                });

                                                notification.sendNotification(apiCreds, subscription_status, plan_id, requestArray['id'], SKUQuantityFlag,requestType, function (err, res) {
                                                    if (!err && res['statusMessage'].includes("OK")) {
                                                        console.log("Notifications Successfully sent");
                                                        logger.debug("Notifications Successfully sent");

                                                    } else {
                                                        console.log("Notification sending failed");
                                                        logger.error("Notification sending failed");
                                                    }
                                                });


                                            } else {
                                                logger.warn("Connection reset exception.");
                                                console.log("Connection reset exception occurred.");
                                            }
                                        }
                                    });
                                } else if (requestArray.asset.items.length === 0) {
                                    console.log("No items on purchase request");
                                    logger.error("No items on purchase request");
                                }
                            } else {
                                console.log("product id not matched ");
                                logger.error("product id not matched ");
                            }
                        } catch (e) {
                            operation.updateRequest("", apiCreds['Request-URL'], apiCreds['VENDOR_API_KEY'], requestArray.id, requestArray.asset.params[0].id, function (err, res) {
                                if (res != null) {
                                    console.log("Updated Successfully");
                                    logger.debug("Updated Successfully");
                                } else {
                                    console.log("Updation failed");
                                    logger.debug("Updation failed");
                                }

                            });

                            operation.inquireRequest(apiCreds['Request-URL'], apiCreds['VENDOR_API_KEY'], requestArray['id'], function (err, res) {
                                if (!err && res['statusMessage'].includes("OK")) {
                                    console.log("Inquired Successfully");
                                    logger.debug("Inquired Successfully");
                                } else {
                                    console.log("Inquire failed");
                                    logger.debug("Inquire failed");
                                }
                            });
                        }


                    });
                }
            } else {
                console.log("List request retrieval failed");
                logger.error("List request retrieval failed");
            }
        });
    } catch (e) {
        logger.error("Error occurred in SDK " + e);
    }

};
