var nodemailer = require('nodemailer');



exports.sendNotification = function (apiCreds,subscription_status,plan_id,requestId,isHybrid,requestType,callback) {
    var transporter = nodemailer.createTransport({
        host: apiCreds['HOST'],
        secure: false,
        auth: {
            user: apiCreds['EMAIL_ID_FROM'],
            pass: apiCreds['PASSWORD']
        }
    });
    var text="";

    if(isHybrid===1) {
        if (subscription_status === 0) {
            text = "Request id " + requestId + " approved((Action:"+requestType+") for manufacturer id " + plan_id + " and Stock Keeping Unit : "+apiCreds['ITEM_SKU']+" . Please approve manually for Manual SKU Plans.";
        } else {
            text = "Request id " + requestId + " failed(Action:"+requestType+") for manufacturer id " + plan_id + " and Stock Keeping Unit : "+apiCreds['ITEM_SKU']+" . Please check whether this account already exists in Docusign.";
        }
    }else{
        text = "Request id " + requestId + " approved(Action:"+requestType+") for manufacturer id " + plan_id+ " and Stock Keeping Unit : "+apiCreds['ITEM_SKU'];
    }

    var mailOptions = {
        from: apiCreds['EMAIL_ID_FROM'],
        to: apiCreds['EMAIL_ID_TO'],
        subject: 'Subscription status',
        text:text
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    }, function (err, res) {
        if (callback) {
            callback(err, res);
        }
    });
};