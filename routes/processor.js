var auth = require("../routes/authentication");
var createAccount = require("../routes/newAccount");
var operation = require("../routes/accountOperations");


var logConfig = '../logConfig.json';
var fileSys = require('fs');
var log = fileSys.readFileSync(logConfig);
var logCreds = JSON.parse(log);
var logger = require(logCreds['LOG_TYPE']).createSimpleLogger(logCreds['LOG_PATH']);
logger.setLevel(logCreds['LOG_MODE']);


exports.processRequest = function (requestData, callback) {

    var i;
    var plan_id = "";
    var SKUQuantityFlag=0;

    exports.token = "";
    var apiConfig = '../config.json';
    var fs = require('fs');
    var private_key = fs.readFileSync(apiConfig);
    var apiCreds = JSON.parse(private_key);
    var responseMessage = null;
    var accountId = requestData.asset.params[0].value;
    for (i = 0; i < requestData['asset'].items.length; i++) {
        if (requestData['asset'].items[i].id===apiCreds['ITEM_SKU']) {
            plan_id = requestData['asset'].items[i].mpn;
        }else{
            if(requestData['asset'].items[i].quantity>0)
                SKUQuantityFlag=1;
        }
    }
    switch (requestData.type) {
        case "suspend":
            try {
                auth.configureJWTAuthorizationFlow(apiCreds.privateKeyFilename, apiCreds.OAuthBaseUrl, apiCreds.PARTNER_ID, apiCreds.CLIENT_id, function (err, res) {
                    if (!err && res['statusMessage'].includes('OK') && JSON.parse(res['body'])) {
                        console.log("Authenticated Successfully");
                        logger.debug("Authenticated Successfully");
                        token = JSON.parse(res['body'])['access_token'];
                        accountId = requestData.asset.params[0].value;
                        operation.suspendAccount(apiCreds.BASE_URL, apiCreds.PARTNER_ID, accountId, token, function (err, res) {
                            if (!err && !res.body.includes("error")) {
                                console.log("suspend Successfully");
                                logger.debug("suspend Successfully");
                                responseMessage = res['body'];
                            } else {
                                console.log("suspend failed");
                                logger.debug("suspend failed");
                                if(res!=null){
                                    err = res['body'];
                                }
                            }

                            callback(err, responseMessage,SKUQuantityFlag,plan_id,requestData.type);
                        });
                    }else{
                        console.log("Not Authenticated");
                        logger.error("Not Authenticated "+err.code);
                    }
                });
                return responseMessage;
            } catch (e) {
                console.log("Exception occurred in suspend ", e);
                logger.error("Exception occurred in suspend ", e);
            }
            break;
        case "resume" :
            try {

                auth.configureJWTAuthorizationFlow(apiCreds.privateKeyFilename, apiCreds.OAuthBaseUrl, apiCreds.PARTNER_ID, apiCreds.CLIENT_id, function (err, res) {
                    if (!err && res['statusMessage'].includes('OK') && JSON.parse(res['body'])) {
                        console.log("Authenticated Successfully");
                        logger.debug("Authenticated Successfully");
                        token = JSON.parse(res['body'])['access_token'];
                        operation.unSuspendAccount(apiCreds.BASE_URL, apiCreds.PARTNER_ID, accountId, token, function (err, res) {
                            if (!err && !res.body.includes("error")) {
                                console.log("resumed Successfully");
                                logger.debug("resumed Successfully");
                                responseMessage = res['body'];
                            } else {
                                logger.debug("resumed failed");
                                console.log("resumed failed");
                                if(res!=null){
                                    err = res['body'];
                                }
                            }
                            callback(err, responseMessage,SKUQuantityFlag,plan_id,requestData.type);
                        });
                    }else{
                        console.log("Not Authenticated");
                        logger.error("Not Authenticated "+err.code);
                    }
                });
            } catch (e) {
                console.log("Error occurred in resume ", e);
                logger.error("Error occurred in resume ", e);

            }
            return responseMessage;
        case "purchase":
            try {
                auth.configureJWTAuthorizationFlow(apiCreds.privateKeyFilename, apiCreds.OAuthBaseUrl, apiCreds.PARTNER_ID, apiCreds.CLIENT_id, function (error, response) {
                    if (!error && response['statusMessage'].includes('OK') && JSON.parse(response['body'])) {
                        console.log("Authenticated Successfully");
                        logger.debug("Authenticated Successfully");
                        token = JSON.parse(response['body'])['access_token'];
                        createAccount.newAccount(plan_id, apiCreds.BASE_URL,apiCreds.PARTNER_ID, token,requestData, function (newAccountError, newAccountResponse) {
                            if (!newAccountError && !newAccountResponse.body.includes("error")) {
                                console.log("account created Successfully");
                                logger.debug("account created Successfully");
                                responseMessage = newAccountResponse['body'];
                                // restoring the account id in the params//
                                var accountId = JSON.parse(newAccountResponse['body']).accounts[0].id;
                                operation.updateRequest(accountId, apiCreds['Request-URL'],apiCreds['VENDOR_API_KEY'], requestData.id, requestData.asset.params[0].id, function (err, res) {
                                    if (res != null && res['statusMessage'].includes('OK')) {
                                        console.log("Updated Successfully");
                                        logger.debug("Updated Successfully");
                                    } else {
                                        console.log("Updation failed with error "+res.body);
                                        logger.error("Updation failed with error "+res.body);
                                    }
                                    callback(newAccountError, responseMessage,SKUQuantityFlag,plan_id,requestData.type);
                                });

                            } else if (!newAccountError && newAccountResponse['body'].includes("partner_account_user_exists")) {
                                console.log("account already exists in Docusign ");
                                logger.debug("account already exists in Docusign ");
                                responseMessage = "partner_account_user_exists";
                                callback(newAccountError, responseMessage,SKUQuantityFlag,plan_id,requestData.type);
                            } else {
                                console.log("account creation failed ");
                                logger.error("account creation failed");
                                if(newAccountResponse!=null){
                                    error = newAccountResponse['body'];
                                }else {
                                    error = newAccountError.code;
                                }
                                callback(error, responseMessage,SKUQuantityFlag,plan_id,requestData.type);
                            }
                           //callback(error, responseMessage);
                        });
                    }else{
                        console.log("Not Authenticated");
                        logger.error("Not Authenticated "+error.code);
                    }

                });

            } catch (e) {
                console.log("checking error", e);
                logger.error("checking error", e);
            }
            break;


        case "cancel":
            try {
                auth.configureJWTAuthorizationFlow(apiCreds.privateKeyFilename, apiCreds.OAuthBaseUrl, apiCreds.PARTNER_ID, apiCreds.CLIENT_id, function (err, res) {
                    if (!err && res['statusMessage'].includes('OK') && JSON.parse(res['body'])) {
                        console.log("Authenticated Successfully");
                        logger.debug("Authenticated Successfully");
                        token = JSON.parse(res['body'])['access_token'];
                        operation.closeAccount(apiCreds.BASE_URL, apiCreds.PARTNER_ID, accountId, token, function (err, res) {
                            if (!err && res['statusMessage'].includes('OK')) {
                                console.log("Canceled Successfully");
                                logger.debug("Canceled Successfully");
                                responseMessage = res['body'];
                            } else {
                                 if(res!=null){
                                     console.log("Canceled failed with error code ", +res.statusCode);
                                     err = res['body'];
                                }
                            }

                            callback(err, responseMessage,SKUQuantityFlag,plan_id,requestData.type);
                        });
                    }else{
                        console.log("Not Authenticated");
                        logger.error("Not Authenticated "+err.code);
                    }
                });
            } catch (e) {
                console.log("Error occurred in cancel", e);
                logger.error("Error occurred in cancel", e);
            }
            return responseMessage;

        case "change":
            try {
                for (i = 0; i < requestData['asset'].items.length; i++) {
                    if (requestData['asset'].items[i].id===apiCreds['ITEM_SKU']) {
                        newseats = requestData.asset.items[i].quantity;
                    }
                }

                auth.configureJWTAuthorizationFlow(apiCreds.privateKeyFilename, apiCreds.OAuthBaseUrl, apiCreds.PARTNER_ID, apiCreds.CLIENT_id, function (err, res) {
                    if (!err && res['statusMessage'].includes('OK') && JSON.parse(res['body'])) {
                        console.log("Authenticated Successfully");
                        logger.debug("Authenticated Successfully");
                        token = JSON.parse(res['body'])['access_token'];
                        operation.modifySeatsAccount(apiCreds.BASE_URL, apiCreds.PARTNER_ID, accountId, newseats, token, function (err, res) {
                            if (!err && !res.body.includes("error")) {
                                console.log("Changed Successfully");
                                logger.debug("Changed Successfully");
                                responseMessage = res['body'];
                            } else {
                                console.log("Change request failed with error code ", +res.statusCode);
                                logger.debug("Change request failed with error code ", +res.statusCode);
                                if(res!=null){
                                    err = res['body'];
                                }
                            }

                            callback(err, responseMessage,SKUQuantityFlag,plan_id,requestData.type);
                        });
                    }else{
                        console.log("Not Authenticated");
                        logger.error("Not Authenticated "+err.code);
                    }
                });
            } catch (e) {
                console.log("Error occurred in change request ", e);
                logger.error("Error occurred in change request ", e);
            }
            return responseMessage;

        default:
            console.log("Unhandled case:");
            logger.error("Unhandled case:" + requestData.type);
            responseMessage = "Unhandled case:" + requestData.type;
            return responseMessage;
    }


};
