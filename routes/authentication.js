var Request = require("request");
var logConfig = '../logConfig.json';
var fileSys = require('fs');
var log = fileSys.readFileSync(logConfig);
var logCreds = JSON.parse(log);
var logger = require(logCreds['LOG_TYPE']).createSimpleLogger(logCreds['LOG_PATH']);
logger.setLevel(logCreds['LOG_MODE']);


exports.configureJWTAuthorizationFlow = function (privateKeyFilename, oAuthBasePath, partnetId, clientId, callback) {

     try {
        var jwt = require('jsonwebtoken')
            , fs = require('fs')
            , private_key = fs.readFileSync(privateKeyFilename)
            , now = Math.floor(Date.now() / 1000)
            , later = now + 360000;

        var jwt_payload = {
            "iss": clientId,
            "aud": oAuthBasePath,
            "iat": now,
            "exp": later,
            "scope": "partner"
        };


        var assertion = jwt.sign(jwt_payload, private_key, {algorithm: 'RS256'});
        Request({
            uri: "https://" + oAuthBasePath + "/oauth/token",
            method: "post",
            timeout: 100000,
            followRedirect: true,
            maxRedirects: 10,
            content: 'application/x-www-form-urlencoded',
            form: {
                assertion: assertion,
                grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',

            }

        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred ", e);
         logger.error("Exception occurred ", e);
    }

};

function hasNoInvalidScopes(scopes) {
    var validScopes = require('./oauth/Scope');

    return (
        Array.isArray(scopes)
        && scopes.length > 0
        && scopes.every(function (scope) {
            return Object.keys(validScopes).some(function (key) {
                return validScopes[key] === scope;
            })
        })
    );
};


