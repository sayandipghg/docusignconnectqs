var Request = require("request");

var logConfig = '../logConfig.json';
var fileSys = require('fs');
var log = fileSys.readFileSync(logConfig);
var logCreds = JSON.parse(log);
var logger = require(logCreds['LOG_TYPE']).createSimpleLogger(logCreds['LOG_PATH']);
logger.setLevel(logCreds['LOG_MODE']);

exports.getAccountInformation = function (BASE_URL, partnerId, accountId, token, callback) {

    var url = BASE_URL + '/subscription/v1/partners/' + partnerId + '/accounts/' + accountId;
    Request({
        "uri": url,
        "method": "get",
        "timeout": 10000,
        "followRedirect": true,
        "maxRedirects": 10,
        "version": 1,
        headers: {
            "Authorization": "Bearer " + token,
            "Content-Type": 'application/json;charset=UTF-8'
        }
    }, function (err, res) {
        if (callback) {
            callback(err, res);
        }
    });
};

exports.closeAccount = function (BASE_URL, partnerId, accountId, token, callback) {
    var url = BASE_URL + '/subscription/v1/partners/' + partnerId + '/accounts/close';
    var accounts = {};
    var key_account = 'accounts';
    accounts[key_account] = [];
    try {
        var account = {
            id: accountId
        };
        accounts[key_account].push(account);
        Request({
            "uri": url,
            "method": "post",
            "timeout": 10000,
            "followRedirect": true,
            "maxRedirects": 10,
            "version": 1,
            "body": JSON.stringify(accounts),
            headers: {
                "Authorization": "Bearer " + token,
                "Content-Type": 'application/json;charset=UTF-8'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in close account ", e);
        logger.error("Exception occurred in close account ", e);
    }
};

exports.unSuspendAccount = function (BASE_URL, partnerId, accountId, token, callback) {
    var url = BASE_URL + '/subscription/v1/partners/' + partnerId + '/accounts/unsuspend';

    var accounts = {};
    var key_account = 'accounts';
    accounts[key_account] = [];
    try {
        var account = {
            id: accountId
        };
        accounts[key_account].push(account);

        Request({
            "uri": url,
            "method": "post",
            "timeout": 10000,
            "followRedirect": true,
            "maxRedirects": 10,
            "version": 1,
            "body": JSON.stringify(accounts),
            headers: {
                "Authorization": "Bearer " + token,
                "Content-Type": 'application/json;charset=UTF-8'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in suspend account ", e);
        logger.error("Exception occurred in suspend account ", e);
    }
};

exports.suspendAccount = function (BASE_URL, partnerId, accountId, token, callback) {
    var url = BASE_URL + '/subscription/v1/partners/' + partnerId + '/accounts/suspend';
    try {
        var accounts = {};
        var key_account = 'accounts';
        accounts[key_account] = [];

        var account = {
            id: accountId
        };
        accounts[key_account].push(account);

        Request({
            "uri": url,
            "method": "post",
            "timeout": 10000,
            "followRedirect": true,
            "maxRedirects": 10,
            "version": 1,
            "body": JSON.stringify(accounts),
            headers: {
                "Authorization": "Bearer " + token,
                "Content-Type": 'application/json;charset=UTF-8'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in suspend function ", e);
        logger.error("Exception occurred in suspend function ", e);
    }

};

exports.modifySeatsAccount = function (BASE_URL, partnerId, accountId, seats, token, callback) {
    var url = BASE_URL + '/subscription/v1/partners/' + partnerId + '/accounts/' + accountId + "/seats";
    try {
        var account = {
            paid: seats,
        };
        Request({
            "uri": url,
            "method": "post",
            "timeout": 10000,
            "followRedirect": true,
            "maxRedirects": 10,
            "version": 1,
            "body": JSON.stringify(account),
            headers: {
                "Authorization": "Bearer " + token,
                "Content-Type": 'application/json;charset=UTF-8'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in update account ", e);
        logger.error("Exception occurred in update account ", e);
    }
};


exports.listRequests = function (APIKEY, RequestURl,vendor_api_key, status, callback) {
    var url = RequestURl + '/public/v1/requests/?status=' + status;
    try {
        Request({
            "uri": url,
            "method": "get",
            "version": 1,
            headers: {
                "Authorization": 'ApiKey ' + vendor_api_key,
                "Content-Type": 'application/json'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in suspend function ", e);
        logger.error("Exception occurred in suspend function ", e);
    }

};

exports.approveRequest = function (template_id,RequestURl,vendor_api_key, requestId, callback) {
    var url = RequestURl + '/public/v1/requests/' + requestId + '/approve';
    try {
        /*var activationTile = {
            activation_tile: "Activation succeeded"
        };*/
        var activationTemplateId = {
            template_id: template_id
        };

        Request({
            "uri": url,
            "method": "post",
            "followRedirect": true,
            "maxRedirects": 10,
            "version": 1,
            "body": JSON.stringify(activationTemplateId),
            headers: {
                "Authorization": 'ApiKey ' + vendor_api_key,
                "Content-Type": 'application/json'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in approve request ", e);
        logger.error("Exception occurred in approve request ", e);
    }

};

exports.failRequest = function (reason, RequestURl, vendor_api_key,requestId, callback) {
    var url = RequestURl + '/public/v1/requests/' + requestId + '/fail';
    try {
        var failTile = {
            "reason": reason
        };
        Request({
            "uri": url,
            "method": "post",
            "followRedirect": true,
            "version": 1,
            "body": JSON.stringify(failTile),
            headers: {
                "Authorization": 'ApiKey ' + vendor_api_key,
                "Content-Type": 'application/json'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in fail request ", e);
        logger.error("Exception occurred in fail request ", e);
    }

};

exports.updateRequest = function (accountId, RequestURl, vendor_api_key,requestId, paramName,callback) {
    var url = RequestURl + '/public/v1/requests/' + requestId;
    var params = {};
    var key_params = 'params';
    params[key_params] = [];

    var keys = {
        id: paramName,
        value: accountId
    };
    params[key_params].push(keys);
    var reqBody = "{\"asset\" :" + JSON.stringify(params) + "}";

    try {
        Request({
            "uri": url,
            "method": "put",
            "followRedirect": true,
            "version": 1,
            "body": reqBody,
            headers: {
                "Authorization": 'ApiKey ' + vendor_api_key,
                "Content-Type": 'application/json'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in update request ", e);
        logger.error("Exception occurred in update request ", e);
    }

};

exports.inquireRequest = function (RequestURl, vendor_api_key,requestId, callback) {
    var url = RequestURl + '/public/v1/requests/' + requestId + '/fail';
    try {
        Request({
            "uri": url,
            "method": "post",
            "version": 1,
            headers: {
                "Authorization": 'ApiKey ' + vendor_api_key,
                "Content-Type": 'application/json'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Exception occurred in fail request ", e);
        logger.error("Exception occurred in fail request ", e);
    }

};
