var Request = require("request");
var logConfig = '../logConfig.json';
var fileSys = require('fs');
var log = fileSys.readFileSync(logConfig);
var logCreds = JSON.parse(log);
var logger = require(logCreds['LOG_TYPE']).createSimpleLogger(logCreds['LOG_PATH']);
logger.setLevel(logCreds['LOG_MODE']);

exports.newAccount = function (plan_id, baseUrl, partnerId,token,request, callback) {
    var url = baseUrl + '/subscription/v1/partners/' + partnerId + '/accounts';
    var accounts = {};
    var key_account = 'accounts';
    var body="";
    accounts[key_account] = [];
    try {
        var countryCodes = {
            "AF": "AFG",
            "AX": "ALA",
            "AL": "ALB",
            "DZ": "DZA",
            "AS": "ASM",
            "AD": "AND",
            "AO": "AGO",
            "AI": "AIA",
            "AQ": "ATA",
            "AG": "ATG",
            "AR": "ARG",
            "AM": "ARM",
            "AW": "ABW",
            "AU": "AUS",
            "AT": "AUT",
            "AZ": "AZE",
            "BS": "BHS",
            "BH": "BHR",
            "BD": "BGD",
            "BB": "BRB",
            "BY": "BLR",
            "BE": "BEL",
            "BZ": "BLZ",
            "BJ": "BEN",
            "BM": "BMU",
            "BT": "BTN",
            "BO": "BOL",
            "BA": "BIH",
            "BW": "BWA",
            "BV": "BVT",
            "BR": "BRA",
            "VG": "VGB",
            "IO": "IOT",
            "BN": "BRN",
            "BG": "BGR",
            "BF": "BFA",
            "BI": "BDI",
            "KH": "KHM",
            "CM": "CMR",
            "CA": "CAN",
            "CV": "CPV",
            "KY": "CYM",
            "CF": "CAF",
            "TD": "TCD",
            "CL": "CHL",
            "CN": "CHN",
            "HK": "HKG",
            "MO": "MAC",
            "CX": "CXR",
            "CC": "CCK",
            "CO": "COL",
            "KM": "COM",
            "CG": "COG",
            "CD": "COD",
            "CK": "COK",
            "CR": "CRI",
            "CI": "CIV",
            "HR": "HRV",
            "CU": "CUB",
            "CY": "CYP",
            "CZ": "CZE",
            "DK": "DNK",
            "DJ": "DJI",
            "DM": "DMA",
            "DO": "DOM",
            "EC": "ECU",
            "EG": "EGY",
            "SV": "SLV",
            "GQ": "GNQ",
            "ER": "ERI",
            "EE": "EST",
            "ET": "ETH",
            "FK": "FLK",
            "FO": "FRO",
            "FJ": "FJI",
            "FI": "FIN",
            "FR": "FRA",
            "GF": "GUF",
            "PF": "PYF",
            "TF": "ATF",
            "GA": "GAB",
            "GM": "GMB",
            "GE": "GEO",
            "DE": "DEU",
            "GH": "GHA",
            "GI": "GIB",
            "GR": "GRC",
            "GL": "GRL",
            "GD": "GRD",
            "GP": "GLP",
            "GU": "GUM",
            "GT": "GTM",
            "GG": "GGY",
            "GN": "GIN",
            "GW": "GNB",
            "GY": "GUY",
            "HT": "HTI",
            "HM": "HMD",
            "VA": "VAT",
            "HN": "HND",
            "HU": "HUN",
            "IS": "ISL",
            "IN": "IND",
            "ID": "IDN",
            "IR": "IRN",
            "IQ": "IRQ",
            "IE": "IRL",
            "IM": "IMN",
            "IL": "ISR",
            "IT": "ITA",
            "JM": "JAM",
            "JP": "JPN",
            "JE": "JEY",
            "JO": "JOR",
            "KZ": "KAZ",
            "KE": "KEN",
            "KI": "KIR",
            "KP": "PRK",
            "KR": "KOR",
            "KW": "KWT",
            "KG": "KGZ",
            "LA": "LAO",
            "LV": "LVA",
            "LB": "LBN",
            "LS": "LSO",
            "LR": "LBR",
            "LY": "LBY",
            "LI": "LIE",
            "LT": "LTU",
            "LU": "LUX",
            "MK": "MKD",
            "MG": "MDG",
            "MW": "MWI",
            "MY": "MYS",
            "MV": "MDV",
            "ML": "MLI",
            "MT": "MLT",
            "MH": "MHL",
            "MQ": "MTQ",
            "MR": "MRT",
            "MU": "MUS",
            "YT": "MYT",
            "MX": "MEX",
            "FM": "FSM",
            "MD": "MDA",
            "MC": "MCO",
            "MN": "MNG",
            "ME": "MNE",
            "MS": "MSR",
            "MA": "MAR",
            "MZ": "MOZ",
            "MM": "MMR",
            "NA": "NAM",
            "NR": "NRU",
            "NP": "NPL",
            "NL": "NLD",
            "AN": "ANT",
            "NC": "NCL",
            "NZ": "NZL",
            "NI": "NIC",
            "NE": "NER",
            "NG": "NGA",
            "NU": "NIU",
            "NF": "NFK",
            "MP": "MNP",
            "NO": "NOR",
            "OM": "OMN",
            "PK": "PAK",
            "PW": "PLW",
            "PS": "PSE",
            "PA": "PAN",
            "PG": "PNG",
            "PY": "PRY",
            "PE": "PER",
            "PH": "PHL",
            "PN": "PCN",
            "PL": "POL",
            "PT": "PRT",
            "PR": "PRI",
            "QA": "QAT",
            "RE": "REU",
            "RO": "ROU",
            "RU": "RUS",
            "RW": "RWA",
            "BL": "BLM",
            "SH": "SHN",
            "KN": "KNA",
            "LC": "LCA",
            "MF": "MAF",
            "PM": "SPM",
            "VC": "VCT",
            "WS": "WSM",
            "SM": "SMR",
            "ST": "STP",
            "SA": "SAU",
            "SN": "SEN",
            "RS": "SRB",
            "SC": "SYC",
            "SL": "SLE",
            "SG": "SGP",
            "SK": "SVK",
            "SI": "SVN",
            "SB": "SLB",
            "SO": "SOM",
            "ZA": "ZAF",
            "GS": "SGS",
            "SS": "SSD",
            "ES": "ESP",
            "LK": "LKA",
            "SD": "SDN",
            "SR": "SUR",
            "SJ": "SJM",
            "SZ": "SWZ",
            "SE": "SWE",
            "CH": "CHE",
            "SY": "SYR",
            "TW": "TWN",
            "TJ": "TJK",
            "TZ": "TZA",
            "TH": "THA",
            "TL": "TLS",
            "TG": "TGO",
            "TK": "TKL",
            "TO": "TON",
            "TT": "TTO",
            "TN": "TUN",
            "TR": "TUR",
            "TM": "TKM",
            "TC": "TCA",
            "TV": "TUV",
            "UG": "UGA",
            "UA": "UKR",
            "AE": "ARE",
            "GB": "GBR",
            "US": "USA",
            "UM": "UMI",
            "UY": "URY",
            "UZ": "UZB",
            "VU": "VUT",
            "VE": "VEN",
            "VN": "VNM",
            "VI": "VIR",
            "WF": "WLF",
            "EH": "ESH",
            "YE": "YEM",
            "ZM": "ZMB",
            "ZW": "ZWE"
        };

        var admin_user = {
            email_address: request.asset.tiers.customer['contact_info'].contact['email'],
            first_name: request.asset.tiers.customer['contact_info'].contact['first_name'],
            last_name: request.asset.tiers.customer['contact_info'].contact['last_name'],
            job_title: ""
        };

        var address = {
            street_address: request.asset.tiers.customer['contact_info'].address_line1,
            locality: request.asset.tiers.customer['contact_info'].city,
            region: request.asset.tiers.customer.contact_info['state'],
            country: countryCodes[request.asset.tiers.customer['contact_info'].country.toUpperCase()],
            postal_code: request.asset.tiers.customer.contact_info['postal_code'],
            phone: request.asset.tiers.customer.contact_info.contact.phone_number['phone_number'],
        };

        if (request.asset.tiers['tier2'].id!=null) {
            resellerid = request.asset.tiers['tier2'].id;
        } else {
            resellerid = request.asset.tiers['tier1'].id;
        }

        var account = {
            address: address,
            admin_user: admin_user,
            name: request.asset.tiers.customer['name'],
            plan_id: plan_id,
            website: "",
            reseller_id: request.asset.tiers['tier1'].id,
            customer_id: request.asset.tiers.customer['id'],
            paid_seats: request.asset.items[0].quantity
        };

        accounts[key_account].push(account);
        logger.debug(JSON.stringify(accounts));
        body=JSON.stringify(accounts);
        Request({
            "uri": url,
            "method": "post",
            "version": 1,
            "body":body,
            headers: {
                "Authorization": "Bearer " + token,
                "Content-Type": 'application/json;charset=UTF-8'
            }
        }, function (err, res) {
            if (callback) {
                callback(err, res);
            }
        });
    } catch (e) {
        console.log("Error occurred ", e);
        logger.error("Error occurred in newAccount ", e);
        var err={
            code:e.message
        };
        callback(err,null);
    }
};






















