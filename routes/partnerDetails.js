function getPartnerDetails(partnerId,baseUrl) {
    var url=baseUrl+'/subscription/v1/partners/'+partnerId;
    Request({
        uri: url,
        method: "get",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10,
    }, function (error, response, body) {
        console.log(body);
    });
}

function getListPartnerPlan(partnerId,baseUrl) {
    var url=baseUrl+'/subscription/v1/partners/'+partnerId+/plans;
    Request({
        uri: url,
        method: "get",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10,
    }, function (error, response, body) {
        console.log(body);
    });
}